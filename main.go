package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"sync"
)

const usage = "serve </path/to/music/dir>"

var nowPlaying = ""

func play(songs []string, wg *sync.WaitGroup, m *sync.Mutex) {
	defer wg.Done()
	i := 0

	for {
		m.Lock()
		nowPlaying = path.Base(songs[i])
		m.Unlock()
		_, err := exec.Command("mpg123", songs[i]).Output()
		if err != nil {
			log.Fatal(err)
		}
		i = (i + 1) % len(songs)
	}
}

func main() {
	if len(os.Args) != 2 {
		log.Fatalln(usage)
	}

	songs, err := filepath.Glob(os.Args[1] + "**/*.mp3")
	if err != nil {
		log.Fatal(err)
	}

	songMutex := new(sync.Mutex)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go play(songs, wg, songMutex)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		songMutex.Lock()
		fmt.Fprint(w, "Now playing: "+nowPlaying)
		songMutex.Unlock()
	})
	log.Println("Serving at: http://0.0.0.0:80")
	log.Fatal(http.ListenAndServe("0.0.0.0:80", nil))
	wg.Wait()
}
